/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based 
    on the listID that is passed to it from the given data in cards.json. Then pass control back 
    to the code that called it by using a callback function.
*/

const data = require('./cards.json');

function getListsByCardsId(cardsId) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            try {
                const lists = data[cardsId];
                resolve(lists);
            } catch (error) {
                reject(new Error("Error in reading data"));
            }
        }, 2000); 
    });
}

module.exports = getListsByCardsId;