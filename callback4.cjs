/* 
    Problem 4: Write a function that will use the previously written functions to get the following
    information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const getBoardById = require('./callback1.cjs');
const getListsByBoardId = require('./callback2.cjs');
const getListsByCardsId = require('./callback3.cjs');

function getThanosBoardInfo() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {

            // Get information from the Thanos boards
            getBoardById('mcu453ed')
                .then(board => {
                    console.log("Thanos Board Information:", board);
                    return getListsByBoardId('mcu453ed');
                })
                .then(lists => {
                    console.log("\nAll Lists for Thanos Board:", lists);

                    // Get all cards for the Mind list simultaneously
                    const mindList = lists.find(list => list.name === 'Mind');

                    if (mindList) {
                        return getListsByCardsId(mindList.id);
                    } else {
                        throw new Error("Mind list not found on Thanos board");
                    }
                })
                .then(cards => {
                    console.log("\nAll Cards for the Mind List:", cards);
                    resolve();
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        }, 2000);
    });
}

module.exports = getThanosBoardInfo;