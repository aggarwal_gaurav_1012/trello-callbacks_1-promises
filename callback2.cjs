/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID 
    that is passed to it from the given data in lists.json. Then pass control back to the code that 
    called it by using a callback function.
*/

const data = require('./lists_1.json');

function getListsByBoardId(boardId) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            try {
                const lists = data[boardId];
                resolve(lists);
            } catch (error) {
                reject(new Error("Error in reading data"));
            }
        }, 2000);
    });
}

module.exports = getListsByBoardId;