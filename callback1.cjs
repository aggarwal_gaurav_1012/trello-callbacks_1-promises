/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID 
    that is passed from the given list of boards in boards.json and then pass control back to the code 
    that called it by using a callback function.
*/

const boards = require('./boards.json');

function getBoardById(boardId) {
    
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let foundBoard = null;
            for (let index = 0; index < boards.length; index++) {
                if (boards[index].id === boardId) {
                    foundBoard = boards[index];
                    break;
                }
            }
            if (foundBoard) {
                resolve(foundBoard);
            } else {
                reject(new Error("Board not found"));
            }
        }, 2000);
    });
}

module.exports = getBoardById;