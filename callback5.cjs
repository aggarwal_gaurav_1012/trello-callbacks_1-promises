/* 
	Problem 5: Write a function that will use the previously written functions to get the following
    information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const getBoardById = require('./callback1.cjs');
const getListsByBoardId = require('./callback2.cjs');
const getListsByCardsId = require('./callback3.cjs');

function getThanosBoardInfo() {

    return new Promise((resolve, reject) => {
        setTimeout(() => {

            // Get information from the Thanos boards
            getBoardById('mcu453ed')
                .then(board => {
                    console.log("Thanos Board Information:", board);
                    return getListsByBoardId('mcu453ed');
                })
                // Get all the lists for the Thanos board
                .then(lists => {
                    console.log("\nAll Lists for Thanos Board:", lists);

                    // Get all cards for the Mind and Space lists simultaneously
                    const mindList = lists.find(list => list.name === 'Mind');
                    const spaceList = lists.find(list => list.name === 'Space');

                    if(mindList && spaceList) {
                        return Promise.all([
                            getListsByCardsId(mindList.id), getListsByCardsId(spaceList.id)
                        ]);
                    } else {
                        throw new Error("Mind and Space list not found");
                    }
                })
                .then(([mindCards, spaceCards]) => {
                    console.log("\nAll cards for the Mind list:", mindCards);
                    console.log("\nAll cards for the Space list:", spaceCards);
                    resolve();
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        }, 2000);
    });
}

module.exports = getThanosBoardInfo;