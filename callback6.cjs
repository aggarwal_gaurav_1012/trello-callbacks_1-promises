/* 
    Problem 6: Write a function that will use the previously written functions to get the following
    information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const getBoardById = require('./callback1.cjs');
const getListsByBoardId = require('./callback2.cjs');
const getListsByCardsId = require('./callback3.cjs');

function getThanosBoardInfo() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let boardInfo;
            getBoardById('mcu453ed')
                .then(board => {
                    console.log("Thanos Board Information:", board);
                    boardInfo = board;
                    return getListsByBoardId('mcu453ed');
                })
                .then(lists => {
                    console.log("\nAll Lists for Thanos Board:", lists);
                    const listPromises = lists.map(list => {
                        return getListsByCardsId(list.id)
                            .then(cards => {
                                if (!cards) {
                                    console.log(`\nAll Cards for ${list.name} List: No cards for this list`);
                                } else {
                                    console.log(`\nAll Cards for ${list.name} List:`, cards);
                                }
                            });
                    });
                    return Promise.allSettled(listPromises);
                })
                .then(results => {
                    for (let i = 0; i < results.length; i++) {
                        const result = results[i];
                        if (result.status === 'rejected') {
                            console.log(result.reason);
                        }
                    }
                    resolve(boardInfo);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        }, 2000);
    });
}

module.exports = getThanosBoardInfo;