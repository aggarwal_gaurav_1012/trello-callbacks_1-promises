const getListsByBoardId = require('../callback2.cjs');

// Passing one boardId from data
const boardId = 'mcu453ed';

getListsByBoardId(boardId)
    .then(lists => {
        console.log("Details belonging to the particular id:", lists);
    })
    .catch(error => {
        console.log(error.message);
    });