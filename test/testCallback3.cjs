const getListsByBoardId = require('../callback3.cjs');

// Passing one boardId from the data
const cardsId = 'jwkh245';

getListsByBoardId(cardsId)
    .then(lists => {
        console.log("Details belonging to that particular id:", lists);
    })
    .catch(error => {
        console.error(error.message);
    });