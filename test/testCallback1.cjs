const getBoardById = require('../callback1.cjs');

// Passing one boardId from the data
const boardId = "mcu453ed";

getBoardById(boardId)
    .then(board => {
        console.log("Board details:", board);
    })
    .catch(error => {
        console.log(error.message);
    });